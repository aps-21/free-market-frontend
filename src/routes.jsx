import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "./pages/home";
import Login from "./pages/login";

import CategoriaListagem from "./pages/categorias/listagem";
import CadastroCategoria from "./pages/categorias/cadastro";

import ProdutoListagem from "./pages/produtos/listagem";
import CadastroProduto from "./pages/produtos/cadastro";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/login" exact component={Login} />
        <Route path="/categorias" exact component={CategoriaListagem} />
        <Route
          path="/categorias/cadastro"
          exact
          component={CadastroCategoria}
        />
        <Route path="/produtos" exact component={ProdutoListagem} />
        <Route path="/produtos/cadastro" exact component={CadastroProduto} />
      </Switch>
    </BrowserRouter>
  );
}
