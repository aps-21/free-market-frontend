import React from "react";
import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";

import { Link } from "react-router-dom";

import FreemarketLogoHorizontal from "../../assets/img/free_market_h.svg";
import CategoriaListagem from "../../pages/categorias/listagem";

export default function Header() {
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        style={{
          backgroundColor: "#ffc107",
          color: "#333",
        }}
        fixed="top"
      >
        <Container>
          <Navbar.Brand>
            <Link to="/">
              <img
                alt=""
                src={FreemarketLogoHorizontal}
                width="200"
                className="d-inline-block align-top"
              />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown title="Categorias">
                <NavDropdown.Item href="/categorias">Listagem</NavDropdown.Item>
                <NavDropdown.Item href="/categorias/cadastro">
                  Cadastro
                </NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title="Produtos">
                <NavDropdown.Item href="/produtos">Listagem</NavDropdown.Item>
                <NavDropdown.Item href="/produtos/cadastro">
                  Cadastro
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
