import React from "react";
import { Container, Row, Col, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./style.css";

export default function Footer() {
  return (
    <>
      <footer>
        <Navbar>
          <Container>
            <Row>
              <Col sm={8}>
                <Nav defaultActiveKey="/home" as="ul" className="flex-column">
                  <Nav.Item as="li">
                    <Nav.Link>
                      <Link to="/">Home</Link>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item as="li">
                    <Nav.Link>
                      <Link to="/login">Login</Link>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item as="li">
                    <Nav.Link>
                      <Link to="/produto/cadastro">Login</Link>
                    </Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={4}></Col>
            </Row>
          </Container>
        </Navbar>
      </footer>
    </>
  );
}
