import React from "react";
import { Container, Jumbotron, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Apresentacao() {
  return (
    <>
      <Container>
        <Jumbotron>
          <h1 style={{ fontSize: "90px", fontWeight: 300 }}>Bem Vindo!</h1>
          <p
            style={{
              fontSize: "20px",
              marginBottom: "40px",
              marginTop: "20px",
            }}
          >
            Somos a sua plataforma de marketeplace, para ter seu produto
            visualizado com mais de 1.000.000 de usuários basta fazer seu
            cadastro como vendedor, cadastrar o produto que deseja
            comercializar, e ver a mágica começar, o dinheiro não vai para de
            entrar na sua carteira.
          </p>
          <p style={{ fontWeight: 300, marginBottom: "40px" }}>
            Junte-se ao maior meio de vendas online do mundo, e venha faturar
            com a gente.
          </p>
          <p>
            <Button variant="outline-dark" to="/login">
              <Link to="/login" style={{ textDecoration: "none" }}>
                Cadastro
              </Link>
            </Button>
          </p>
        </Jumbotron>
      </Container>
    </>
  );
}
