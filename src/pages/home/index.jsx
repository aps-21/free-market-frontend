import React from "react";
import Header from "../../components/header/Header";
import Apresentacao from "../../components/apresentacao/Apresentacao";
import ProdutosSessao from "../../components/cards/ProdutoSessao";
import Footer from "../../components/footer/Footer";

export default function Home() {
  return (
    <>
      <Header />
      <main>
        <Apresentacao />
        <ProdutosSessao />
      </main>
      <Footer />
    </>
  );
}
