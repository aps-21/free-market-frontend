import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import api from "../../../services/api";

import { Container, Col, Form, Button } from "react-bootstrap";

export default function CadastroProduto() {
  const [nome, setNome] = useState("");
  const [valor, setValor] = useState("");
  const [descricao, setDescricao] = useState("");
  const [categoria_id, setCategoria_id] = useState("");
  const [imagem, setImagem] = useState("");

  const history = useHistory();

  async function handleProduto(e) {
    e.preventDefault();
    const data = { nome, valor, imagem, descricao, categoria_id };

    try {
      const response = await api.post("produtos", data);

      alert(
        `Cadastro realizado com sucesso do produto: ${response.data.nome} e de id: ${response.data.id}`
      );
      setNome("");
      setValor("");
      setImagem("");
      setDescricao("");
      setCategoria_id("");

      history.push("/produtos");
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <>
      <Header />
      <main>
        <Container>
          <br />
          <h1>Cadastro Produto</h1>
          <hr />

          <Form onSubmit={handleProduto}>
            <Form.Row>
              <Form.Group as={Col} controlId="formGridNome">
                <Form.Label>Nome:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Nome do Produto"
                  value={nome}
                  onChange={(e) => setNome(e.target.value)}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridPassword">
                <Form.Label>Valor:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="R$ 000,00"
                  value={valor}
                  onChange={(e) => setValor(e.target.value)}
                />
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formGridImagem">
                <Form.Label>Imagem:</Form.Label>
                <Form.Control
                  placeholder="http://localhost:3000/imagem"
                  value={imagem}
                  onChange={(e) => setImagem(e.target.value)}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridPassword">
                <Form.Label>Categoria Id:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="categoria id"
                  value={categoria_id}
                  onChange={(e) => setCategoria_id(e.target.value)}
                />
              </Form.Group>
            </Form.Row>

            <Form.Group controlId="formDescricao">
              <Form.Label>Descricao:</Form.Label>
              <Form.Control
                as="textarea"
                rows="3"
                value={descricao}
                onChange={(e) => setDescricao(e.target.value)}
              />
            </Form.Group>

            <Button type="submit" size="lg" block variant="outline-dark">
              Salvar
            </Button>
          </Form>
        </Container>
      </main>
      <Footer />
    </>
  );
}
