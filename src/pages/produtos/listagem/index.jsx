import React, { useState, useEffect } from "react";
import { FaRegTrashAlt, FaSyncAlt } from "react-icons/fa";
import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import api from "../../../services/api";
import { Container, CardColumns, Card, Button } from "react-bootstrap";

export default function Listagem() {
  const [produtos, setProdutos] = useState([]);
  localStorage.setItem("vendedorName", "João");
  const vendedorName = localStorage.getItem("vendedorName");

  useEffect(() => {
    api.get("produtos").then((response) => {
      console.log(response.data);
      setProdutos(response.data);
    });
  }, []);

  async function handleDeleteProduto(id) {
    try {
      await api.delete(`produtos/${id}`);
      setProdutos(produtos.filter((produto) => produto.id !== id));
    } catch (error) {
      alert("Erro ao deletar produto.");
    }
  }
  return (
    <>
      <Header />
      <main>
        <Container>
          <h1>Listagem de Produtos da loja {vendedorName}</h1>

          <div style={{ marginTop: "30px" }}>
            <CardColumns>
              {produtos.map((produto) => (
                <Card
                  border="warning"
                  style={{ width: "18rem" }}
                  key={produto.id}
                >
                  <Card.Img variant="top" src={produto.imagem} />
                  <Card.Body>
                    <Card.Title>{produto.nome}</Card.Title>
                    <Card.Text>{produto.descricao}</Card.Text>
                    <hr />
                    <Card.Text>R$ {produto.valor}</Card.Text>
                    <Button
                      onClick={() => handleDeleteProduto(produto.id)}
                      variant="danger"
                      style={{ marginRight: "10px" }}
                    >
                      <FaRegTrashAlt />
                    </Button>
                    <Button variant="primary">
                      <FaSyncAlt />
                    </Button>
                  </Card.Body>
                </Card>
              ))}
            </CardColumns>
          </div>
        </Container>
      </main>
      <Footer />
    </>
  );
}
