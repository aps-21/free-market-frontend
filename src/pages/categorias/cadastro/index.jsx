import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import api from "../../../services/api";

import { Container, Col, Form, Button } from "react-bootstrap";

export default function CadastroCategoria() {
  const vendedorName = localStorage.getItem("vendedorName");
  const [nome, setNome] = useState("");

  const history = useHistory();

  async function handleCategoria(e) {
    e.preventDefault();
    const data = { nome };

    try {
      const response = await api.post("categorias", data);

      alert(
        `Cadastro realizado com sucesso da categoria: ${response.data.nome} e de id: ${response.data.id}`
      );
      setNome("");
      history.push("/categorias");
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <>
      <Header />
      <main>
        <Container>
          <br />
          <h1>Categoria Cadastro Loja {vendedorName}</h1>
          <br />
          <h3>Cadastre a categoria de produtos.</h3>
          <p>
            Categoriazar seus produtos ajudara a criar filtos e facilitará aos
            seus usuários encontra o que desejam mais rápido.
          </p>
          <Form onSubmit={handleCategoria}>
            <Form.Group>
              <Form.Row>
                <Form.Label column="lg" lg={2}>
                  Categoria:
                </Form.Label>
                <Col>
                  <Form.Control
                    size="lg"
                    type="text"
                    placeholder="Nome da Categoria"
                    value={nome}
                    onChange={(e) => setNome(e.target.value)}
                  />
                </Col>
              </Form.Row>
              <br />
            </Form.Group>
            <Button type="submit" size="lg" block variant="outline-dark">
              Cadastrar
            </Button>
          </Form>
        </Container>
      </main>
      <Footer />
    </>
  );
}
