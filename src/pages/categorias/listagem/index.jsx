import React, { useState, useEffect } from "react";
import { Container, CardColumns, Card, Button } from "react-bootstrap";
import { FaRegTrashAlt, FaSyncAlt } from "react-icons/fa";

import api from "../../../services/api";
import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

export default function CategoriaListagem() {
  const [categorias, setCategorias] = useState([]);
  localStorage.setItem("vendedorName", "João");
  const vendedorName = localStorage.getItem("vendedorName");

  useEffect(() => {
    api.get("categorias").then((response) => {
      console.log(response.data);

      setCategorias(response.data);
    });
  }, []);

  async function handleDeleteCategoria(id) {
    try {
      await api.delete(`categorias/${id}`);
      setCategorias(categorias.filter((categoria) => categoria.id !== id));
    } catch (error) {
      alert(
        "Erro ao deletar categoria, verifique se não produtos associados a ela."
      );
    }
  }

  return (
    <>
      <Header />
      <main>
        <Container>
          <h1>Loja {vendedorName} listagem de Categoria </h1>
          <div style={{ marginTop: "30px" }}>
            <CardColumns>
              {categorias.map((categoria) => (
                <Card
                  border="warning"
                  style={{ width: "18rem" }}
                  key={categoria.id}
                >
                  <Card.Header>Categoria</Card.Header>
                  <Card.Body>
                    <Card.Title>{categoria.nome}</Card.Title>
                    {/* <Card.Text>
                  Some quick example text to build on the card title and make up
                  the bulk of the card's content.
                </Card.Text> */}
                    <Button
                      onClick={() => handleDeleteCategoria(categoria.id)}
                      variant="danger"
                      style={{ marginRight: "10px" }}
                    >
                      <FaRegTrashAlt />
                    </Button>
                    <Button variant="primary">
                      <FaSyncAlt />
                    </Button>
                  </Card.Body>
                </Card>
              ))}
            </CardColumns>
          </div>
        </Container>
      </main>
      <Footer />
    </>
  );
}
