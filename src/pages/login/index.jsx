import React from "react";
import { Link } from "react-router-dom";
import { FaRegUser, FaUnlockAlt, FaReplyAll } from "react-icons/fa";
import freeMarket from "../../assets/img/free_market.svg";
import "./style.css";

export default function Login() {
  return (
    <div className="login-container">
      <div className="container-esquerdo">
        <section className="brand-container">
          <img src={freeMarket} alt="Free Market" />
        </section>
      </div>
      <div className="container-direito">
        <section className="form">
          <div className="info-page">
            <h3>Log In</h3>
            <h3>Forgot Password</h3>
          </div>

          <form>
            <div className="input input-email">
              <FaRegUser
                size={20}
                color="#727C8E"
                className="icone"
                style={{ marginTop: 17 }}
              />
              <div>
                <label>email</label>
                <input type="email" placeholder="marcio@email.com" />
              </div>
            </div>
            <div className="input input-password">
              <FaUnlockAlt
                size={20}
                color="#727C8E"
                style={{ marginTop: 17 }}
              />
              <div>
                <label> password</label>
                <input type="password" />
              </div>
            </div>
            <div className="input-button">
              <button>Log in</button>
            </div>
          </form>
          <div className="info-return-page">
            <Link to="/">
              <FaReplyAll size={16} color="#333" /> Volta para home
            </Link>
          </div>
        </section>
      </div>
    </div>
  );
}
